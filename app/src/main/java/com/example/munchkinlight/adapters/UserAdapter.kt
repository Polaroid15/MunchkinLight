package com.example.munchkinlight.adapters

import android.graphics.Color
import android.net.Uri
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.selection.ItemDetailsLookup
import androidx.recyclerview.selection.SelectionTracker
import androidx.recyclerview.widget.RecyclerView
import com.example.munchkinlight.R
import com.example.munchkinlight.data.User

class UserAdapter : RecyclerView.Adapter<UserAdapter.UserHolder>() {

    private var users: List<User> = ArrayList()
    private var tracker: SelectionTracker<Long>? = null

    init {
        setHasStableIds(true)
    }

    fun setTracker(tracker: SelectionTracker<Long>?) {
        this.tracker = tracker
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.user_item, parent, false)

        return UserHolder(view)
    }

    override fun onBindViewHolder(holder: UserHolder, position: Int) {
        val item = users[position]
        if (item.avatar.isEmpty()) {
            setDefaultAvatar(holder)
        }

        tracker?.let {
            if (it.isSelected(position.toLong())) {
                it.select(position.toLong())
                holder.avatarView.setBackgroundColor(Color.parseColor("#FFFFFF"))
            } else {
                it.deselect(position.toLong())

                holder.avatarView.setBackgroundColor(Color.parseColor("#FFF000"))
            }
        }
    }

    private fun setDefaultAvatar(holder: UserHolder) {
        holder.avatarView.setImageURI(null)
        val path = "android.resource://com.example.munchkinlight/"
        val defaultAva: Uri = Uri.parse(path + R.drawable.ic_avatar_mock_24dp)
        holder.avatarView.setImageURI(defaultAva)
    }

    override fun getItemCount(): Int = users.size

    override fun getItemId(position: Int): Long = position.toLong()

    fun refreshUsers(it: List<User>) {
        users = it
        notifyDataSetChanged()
    }

    inner class UserHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val avatarView: AppCompatImageView
        val nameView: TextView
        val sexView: TextView

        init {
            avatarView = itemView.findViewById(R.id.imgView_user_item_ava_icon)
            nameView = itemView.findViewById(R.id.tv_user_item_name)
            sexView = itemView.findViewById(R.id.tv_user_item_sex)
        }

        fun bind(user: User, isActivated: Boolean = false) {
            avatarView.setImageResource(R.drawable.ic_avatar_mock_24dp)
            nameView.text = user.name
            sexView.text = user.sex
            itemView.isActivated = isActivated
        }

        fun getItemDetails(): ItemDetailsLookup.ItemDetails<Long> =
            object : ItemDetailsLookup.ItemDetails<Long>() {
                override fun getPosition(): Int = adapterPosition
                override fun getSelectionKey(): Long? = itemId
            }
    }
}

class MyItemDetailsLookup(private val recyclerView: RecyclerView) :
    ItemDetailsLookup<Long>() {
    override fun getItemDetails(event: MotionEvent): ItemDetails<Long>? {
        val view = recyclerView.findChildViewUnder(event.x, event.y)
        if (view != null) {
            return (recyclerView.getChildViewHolder(view) as UserAdapter.UserHolder)
                .getItemDetails()
        }
        return null
    }
}