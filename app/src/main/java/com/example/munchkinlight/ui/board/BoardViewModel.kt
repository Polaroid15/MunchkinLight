package com.example.munchkinlight.ui.board

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.munchkinlight.data.User
import com.example.munchkinlight.data.UserData

class BoardViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "Name"
    }
    val text: LiveData<String> = _text

    private val _name = MutableLiveData<String>().apply {
        value = "This is board Fragment"
    }
    val name: LiveData<String> = _name

    private val _userList = MutableLiveData<List<User>>().apply {
        value = UserData.getUsers()
    };

    val userList: MutableLiveData<List<User>> = _userList;
}