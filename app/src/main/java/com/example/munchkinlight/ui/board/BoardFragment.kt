package com.example.munchkinlight.ui.board

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.selection.ItemKeyProvider
import androidx.recyclerview.selection.SelectionPredicates
import androidx.recyclerview.selection.SelectionTracker
import androidx.recyclerview.selection.StorageStrategy
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.munchkinlight.R
import com.example.munchkinlight.adapters.MyItemDetailsLookup
import com.example.munchkinlight.adapters.UserAdapter
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton
import com.google.android.material.snackbar.Snackbar


class BoardFragment : Fragment() {

    private lateinit var boardViewModel: BoardViewModel
    private lateinit var user_info_view: View
    private lateinit var userList: RecyclerView
    private lateinit var userAdapter: UserAdapter
    private var tracker: SelectionTracker<Long>? = null

    private var isActiveUser: Boolean = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        boardViewModel = ViewModelProvider(this).get(BoardViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_board, container, false)
        val add_button: ExtendedFloatingActionButton = root.findViewById(R.id.fab_add_user)
        user_info_view = root.findViewById(R.id.rl_current_user)
        userList = root.findViewById(R.id.rv_users)
        userAdapter = UserAdapter()

        add_button.setOnClickListener(View.OnClickListener {
            if (!user_info_view.isVisible) {
                user_info_view.visibility = View.VISIBLE
                userList.visibility = View.GONE
            } else {
                userList.visibility = View.VISIBLE
                user_info_view.visibility = View.GONE
            }

            Snackbar.make(it, "Here's a Snackbar", Snackbar.LENGTH_LONG)
                .setAction("Action", null)
                .show()
        })

        with(userList) {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            adapter = userAdapter
        }

        boardViewModel.userList.observe(viewLifecycleOwner, Observer {
            it?.let {
                userAdapter.refreshUsers(it)
            }
        })
        trackSelectedItems()
//        val textView: TextView = root.findViewById(R.id.text_board)

//        boardViewModel.text.observe(viewLifecycleOwner, Observer {
//            textView.text = it
//        })
//        val nameView: TextView = root.findViewById(R.id.name_board)
//        boardViewModel.name.observe(viewLifecycleOwner, Observer {
//            nameView.text = it;
//        })
        return root
    }

    private fun trackSelectedItems() {
        tracker = SelectionTracker.Builder<Long>(
            "selection-1",
            userList,
            ItemIdKeyProvider(userList),
            MyItemDetailsLookup(userList),
            StorageStrategy.createLongStorage()
        ).withSelectionPredicate(SelectionPredicates.createSelectAnything())
            .build()

        userAdapter.setTracker(tracker)

        tracker?.addObserver(object : SelectionTracker.SelectionObserver<Long>() {
            override fun onSelectionChanged() {
                Log.d("TAG", "SelectionChanged")
                //handle the selected according to your logic
            }
        })
    }

    inner class ItemIdKeyProvider(private val recyclerView: RecyclerView) :
        ItemKeyProvider<Long>(SCOPE_MAPPED) {

        override fun getKey(position: Int): Long? {
            return recyclerView.adapter?.getItemId(position)
                ?: throw IllegalStateException("RecyclerView adapter is not set!")
        }

        override fun getPosition(key: Long): Int {
            val viewHolder = recyclerView.findViewHolderForItemId(key)
            return viewHolder?.layoutPosition ?: RecyclerView.NO_POSITION
        }
    }
}