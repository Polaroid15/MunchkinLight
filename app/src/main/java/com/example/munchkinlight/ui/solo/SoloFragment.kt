package com.example.munchkinlight.ui.solo

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.munchkinlight.R
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton
import com.google.android.material.snackbar.Snackbar

class SoloFragment : Fragment() {

    private lateinit var soloViewModel: SoloViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        soloViewModel =
            ViewModelProvider(this).get(SoloViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_solo, container, false)
        val add_button: ExtendedFloatingActionButton = root.findViewById(R.id.fab_add_solo_user)
        val user_info_view: View = root.findViewById(R.id.user_solo_info)
        val ll_mock: View = root.findViewById(R.id.ll_solo_mock)
        val user_avatar_view: AppCompatImageView = root.findViewById(R.id.imgView_solo_add_avatar)
        val add_user_view: View = root.findViewById(R.id.fragment_solo_add_user)
        val textViewAddName: TextView = root.findViewById(R.id.tv_solo_add_name)
        val textViewNameProfile: TextView = root.findViewById(R.id.tv_solo_info_name)

        user_avatar_view.setOnClickListener(View.OnClickListener {
            Snackbar.make(it, "change ava", Snackbar.LENGTH_SHORT)
                .setAction("Action", null)
                .show()
        })

        add_button.setOnClickListener(View.OnClickListener {
            if (!add_user_view.isVisible && isEmptyUser) {
                ll_mock.visibility = View.GONE
                user_info_view.visibility = View.GONE
                add_user_view.visibility = View.VISIBLE
                isEmptyUser = false
                add_button.setIconResource(R.drawable.ic_recovery_24dp)
                Snackbar.make(it, "Adding new user", Snackbar.LENGTH_LONG)
                    .setAction("Action", null)
                    .show()
            } else if (!isEmptyUser) {
                add_user_view.visibility = View.INVISIBLE
                user_info_view.visibility = View.VISIBLE
                isEmptyUser = true
                Snackbar.make(it, "Open current user", Snackbar.LENGTH_LONG)
                    .setAction("Action", null)
                    .show()
            }
        })

        soloViewModel.text.observe(viewLifecycleOwner, Observer {
            textViewNameProfile.text = it
        })
        return root
    }

    private var isEmptyUser: Boolean = true
}