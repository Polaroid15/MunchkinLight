package com.example.munchkinlight.ui.solo

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class SoloViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "Name"
    }
    val text: LiveData<String> = _text

    private val _avatar = MutableLiveData<String>().apply {
        value = "#FFF000"
    }

    val avatar: LiveData<String> = _avatar
}