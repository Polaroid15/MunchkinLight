package com.example.munchkinlight.ui.settings

import android.os.Bundle
import android.os.Debug
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.appcompat.widget.SwitchCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.munchkinlight.R
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import kotlin.math.log

class SettingsFragment : Fragment() {

    private lateinit var settingsViewModel: SettingsViewModel
    private lateinit var imgButton: ImageButton
    private var isFlag: Boolean = false

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        settingsViewModel =
                ViewModelProvider(this).get(SettingsViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_settings, container, false)
        val imgButton: FloatingActionButton = root.findViewById(R.id.img_button_profile)
        val switchButton: SwitchCompat =  root.findViewById(R.id.switch_screen_settings)

        switchButton.setOnClickListener { v ->
            if(switchButton.isChecked) {
                Snackbar.make(v, "Is checked", Snackbar.LENGTH_LONG)
                    .setAction("Action", null)
                    .show()
            } else {
                Snackbar.make(v, "is Unchecked", Snackbar.LENGTH_LONG)
                    .setAction("Action", null)
                    .show()
            }
            settingsViewModel.isChecked(switchButton.isChecked)
        }

        imgButton.setOnClickListener { v ->
            if(isFlag){
                imgButton.setImageResource(R.drawable.ic_arrow_down_drop_24dp)
            } else {
                imgButton.setImageResource(R.drawable.ic_arrow_up_drop_24dp)
            }
            isFlag = !isFlag
        }
        settingsViewModel.isChecked.observe(viewLifecycleOwner, Observer {
            switchButton.isChecked = it
        })
        return root
    }
}