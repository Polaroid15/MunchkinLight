package com.example.munchkinlight.ui.settings

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class SettingsViewModel : ViewModel() {

    val isChecked = MutableLiveData<Boolean>()

    fun isChecked(b: Boolean) {
        isChecked.value = b
    }
}