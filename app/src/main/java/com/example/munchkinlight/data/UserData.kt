package com.example.munchkinlight.data

object UserData {

    fun getUsers() = listOf(
        User("first user", "male"),
        User("second user", "female"),
        User("third username123456789123456789123456789123456789321", "male"),
        User("name123456789123456789123456789123456789321forth user", "female"),
        User("thisdfsrd 2342sf", "female"),
        User("name123456789123456789123456789123456789321forth user", "female"),
        User("2346bfigfth user", "male"),
        User("first user", "male"),
        User("second user", "female"),
        User("third username123456789123456789123456789123456789321", "male"),
        User("name123456789123456789123456789123456789321forth user", "female"),
        User("thisdfsrd 2342sf", "female"),
        User("name123456789123456789123456789123456789321forth user", "female"),
        User("2346bfigfth user", "male")
    )
}